// This is main process of Electron, started as first thing when your
// app starts. It runs through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.
import {app} from "electron";
import createWindow from "./helpers/window";


// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from "env";

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}


app.on("ready", () => {
  let appArg;
try {
  appArg = process.argv.length > 1 ? process.argv[1] : "";
} catch (e) {
  appArg = "";
}
  let url = 'http://is.techuni.tj';
  if(appArg && appArg.indexOf('http') != -1){
    url = appArg;
  }
  const mainWindow = createWindow("main", {
    width: 1000,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      devTools: false
    }
  });
  const userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) _desk-app/0.0.1 AppleWebKit/537.36 (KHTML, like Gecko) electron/1.0.0 Chrome/53.0.2785.113 Electron/1.4.3 Safari/537.36';
  mainWindow.setMenu(null);
  // mainWindow.loadURL('http://asu.techuni.tj', {userAgent: userAgent});
  // mainWindow.loadURL('http://is.techuni.tj', {userAgent: userAgent});
  mainWindow.loadURL(url, {userAgent: userAgent});
  /*if (env.name === "development") {
    mainWindow.openDevTools();
  }*/
 //mainWindow.openDevTools();
});

app.on("window-all-closed", () => {
  app.quit();
});
